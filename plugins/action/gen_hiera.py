from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from os import path, walk
import re


from ansible.errors import AnsibleError, AnsibleFileNotFound, AnsibleAction, AnsibleActionFail

from ansible.module_utils._text import to_native
from ansible.plugins.action import ActionBase

from ansible.inventory.manager import InventoryManager
from ansible import constants as C
from ansible.plugins.action import ActionBase
from ansible.template import generate_ansible_template_vars
from netaddr import *
import json



class ActionModule(ActionBase):

    TRANSFERS_FILES = False

    vmkeys = [  
        'name',
        'internal_ip', 
        'internal_net',
        'external_net',
        'external_ip',
        'rootsize',
        'internal_tcp_services',
        'internal_custom_tcp_rules',
        'external_tcp_services',
        'kernel',
      ]

    def vmsfromhosts(self, hosts, hypervisor, networks):
        vms = dict()
        for (hostname, data) in hosts.items():
          vm = dict()

          #print(hostname)
          if 'hypervisor' in data:
            if data['hypervisor'] == hypervisor:
                vm['name'] = hostname
                if 'internal_net' in data:
                  int_net = IPNetwork(networks[data['internal_net']]['network'])
                  vm['internal_nm'] = str(int_net.netmask)
                  #vm['internal_network_addr'] = str(int_net.network)
                  #vm['internal_broadcast'] = str(int_net.broadcast)
                  vm['internal_ip'] = str(list(int_net)[data['internal_host']])
                  #vm['netmask'] = str(int_net.netmask)
                  #vm['network_addr'] = str(int_net.network)
                  #vm['broadcast'] = str(int_net.broadcast)
                  #vm['ip'] = str(list(int_net)[data['internal_host']]) 
                  if 'gateway' in data['internal_net']:
                    vm['internal_gw'] = networks[data['internal_net']].gateway
                    #vm['gateway'] = networks[data['internal_net']].gateway

                if 'external_net' in data:
                  ext_net = IPNetwork(networks[data['external_net']]['network'])
                  vm['external_nm'] = str(ext_net.netmask)
                  #vm['external_network_addr'] = str(ext_net.network)
                  #vm['external_broadcast'] = str(ext_net.broadcast)
                  vm['external_ip'] = str(list(ext_net)[data['external_host']])
                  if 'gateway' in data['external_net']:
                    vm['external_gw'] = networks[data['external_net']].gateway

                for (key, value) in data.items():
                  if key in self.vmkeys:
                    vm[key] = value
                vms[vm['name']] = vm

        return vms



    def run(self, tmp=None, task_vars=None):
        """ Generate's hiera data
        """
        ans_hostname = str(task_vars['inventory_hostname'])
        ans_hostvars = task_vars['vars']['hostvars']

        vms = self.vmsfromhosts(ans_hostvars, ans_hostname, task_vars['networks'])

        services = dict()
        for (k, v) in task_vars['services'].items():
            if ans_hostname in v['hosts']:
                services[k] = v

        if not task_vars:
            task_vars = dict()
   
        hiera = dict()
        hiera['vms'] = vms
        hiera['networks'] = task_vars['networks']
        hiera['services'] = services

        result = super(ActionModule, self).run(tmp, task_vars)

        result['hiera'] = hiera
        result['changed'] = True
        result['ivhost'] = str(task_vars['inventory_hostname'])
        result['ser'] = str(task_vars['float_enabled_services'])


        return result


